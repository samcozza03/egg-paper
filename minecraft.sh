        
#!/bin/bash
sleep 1s
echo -e "==============================="
sleep 1s
echo -e "| RAM: $SERVER_MEMORY MB"
sleep 1s
echo -e "| IP: $SERVER_IP:$SERVER_PORT"
sleep 1s
echo -e "| CPU: $(lscpu | sed -nr '/Model name/ s/.*:\s*(.*) @ .*/\1/p')"
sleep 1s
echo -e "==============================="
sleep 2s
echo -e "==============================="
sleep 1s
echo -e "| To backup server, create a file named backup.me"
sleep 1s
echo -e "==============================="
sleep 2s
echo -e "Starting...."

if [ -e "backup.me" ]
then 
    BACKUP="backup.me"
    echo -e "Found "$BACKUP
    echo -e "Processing data please wait"
    zip -r backup.zip *
    curl -F "file=@backup.zip" https://api.anonfile.com/upload
    echo -e "Data processed"
    rm backup.zip backup.me
    sleep 1m
    exit
fi

if [ -e "PocketNode" ]
then 
    POCKETNODE="PocketNode"
    echo -e "Found "$POCKETNODE
    echo -e "Starting PocketNodeX server"
    ./PocketNode
    sleep 10s
    exit
fi

if [ -e "PocketMine-MP.phar" ]
then 
    PMMP="PocketMine-MP.phar"
    echo -e "Found "$PMMP
    echo -e "Starting PocketMineMP server"
    ./bin/php7/bin/php ./PocketMine-MP.phar --no-wizard --disable-ansi
    sleep 10s
    exit
fi

if [ -e "bedrock_server" ]
then 
    BEDROCK="bedrock_server"
    echo -e "Found "$BEDROCK
    echo -e "Starting Vanilla Bedrock  server"
    LD_LIBRARY_PATH=. ./bedrock_server
    sleep 5s
    exit
fi

if [ -e "Cuberite" ]
then 
    CUBERITE="Cuberite"
    echo -e "Found "$CUBERITE
    echo -e "Starting Cuberite server"
    ./Cuberite
    sleep 10s
    exit
fi

if [ -e "discord-bot.js" ]
then 
    DISCORD="discord-bot.js"
    echo -e "Found "$DISCORD
    echo -e "Starting NodeJS  Process"
    npm install --no-progress --unsafe-perm
    node $DISCORD
    sleep 10s
    exit
fi

if [ -e "discord-bot.py" ]
then 
    PYTHON_BOT="discord-bot.py"
    echo -e "Found "$PYTHON_BOT
    echo -e "Starting Python3 Process"
    pip3 install -r requirements.txt --user
    python3 $PYTHON_BOT 
    sleep 10s
    exit
fi

if [ -e "server.jar" ]
then 
    JAR="server.jar"
    echo -e "Found "$JAR
    minimumsize=100000
    actualsize=$(wc -c <"$JAR")
    if [ $actualsize -lt $minimumsize ]
    then
        echo -e $JAR" is not a valid size"
		rm "$JAR"
        JAR=""
    fi
else
    JAR=""
    echo -e "Could not find server.jar"
fi

if [ "$JAR" = "" ]
then
    echo -e "Please choose the number and press enter or send, and it will download it for you."
    PS3='(1-45): '
    options=("Paper 1.15 |" "Paper 1.14.4 |" "Paper 1.13.2 |" "Paper 1.12.2 |" "Paper 1.11.2 |" "Paper 1.10.2 |" "Paper 1.9.4 |" "Paper 1.8.x |" "Paper 1.7.10 |" "Spigot 1.7.10 |" "Spigot 1.8.8 |" "Spigot 1.9.4 |" "Spigot 1.10.2 |" "Spigot 1.11.2 |" "Spigot 1.12.2 |" "Spigot 1.13.2 |" "Spigot 1.14.4 |" "Spigot 1.15 |" "Spigot 1.15.1 |" "Taco 1.8.8 |" "Taco 1.11.2 |" "Taco 1.12.2 |" "Vanilla 1.7.10 |" "Vanilla 1.8.8 |" "Vanilla 1.9.4 |" "Vanilla 1.10.2 |" "Vanilla 1.11.2 |" "Vanilla 1.12.2 |" "Vanilla 1.13.2 |" "Vanilla 1.14.4 |" "Vanilla 1.15 |" "Vanilla 1.15.1 |" "Nukkit |" "Magma 1.12.2 |" "BungeeCord |" "Waterfall |" "NodeJS |" "Python |" "Travertine |" "Velocity |" "PMMP |" "Cuberite |" "Vanilla Bedrock |" "PocketNodeX |" "Custom")
    select opt in "${options[@]}"
    do
        case $opt in
            "Paper 1.15 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Paper-1.15/lastSuccessfulBuild/artifact/paperclip.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Paper 1.14.4 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Paper-1.14/lastSuccessfulBuild/artifact/paperclip.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Paper 1.13.2 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Paper-1.13/lastSuccessfulBuild/artifact/paperclip.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Paper 1.12.2 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Paper/lastSuccessfulBuild/artifact/paperclip.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Paper 1.11.2 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Paper/1104/artifact/paperclip.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Paper 1.10.2 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Paper/916/artifact/paperclip-916.2.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Paper 1.9.4 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Paper/773/artifact/paperclip.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Paper 1.8.x |")
                echo -e "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Paper/443/artifact/Paperclip.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Paper 1.7.10 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://yivesmirror.com/files/paper/PaperSpigot-1.7.10-R0.1-SNAPSHOT-latest.jar" -o server.jar 2> /dev/null > /dev/null
				break        
                ;;
            "Spigot 1.7.10 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.7.10-SNAPSHOT-b1657.jar" -o server.jar 2> /dev/null > /dev/null
				break                
                ;;
            "Spigot 1.8.8 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.8.8-R0.1-SNAPSHOT-latest.jar" -o server.jar 2> /dev/null > /dev/null
				break  
                ;;
            "Spigot 1.9.4 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.9.4-R0.1-SNAPSHOT-latest.jar" -o server.jar 2> /dev/null > /dev/null
				break                       
                ;;
            "Spigot 1.10.2 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.10.2-R0.1-SNAPSHOT-latest.jar" -o server.jar 2> /dev/null > /dev/null
				break    
                ;;      
            "Spigot 1.11.2 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.11.2.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;    
            "Spigot 1.12.2 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.12.2.jar" -o server.jar 2> /dev/null > /dev/null
				break 
                ;;
            "Spigot 1.13.2 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.13.2.jar" -o server.jar 2> /dev/null > /dev/null
				break 
                ;;
            "Spigot 1.14.4 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.14.4.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;    
            "Spigot 1.15 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.15.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;   
            "Spigot 1.15.1 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://cdn.getbukkit.org/spigot/spigot-1.15.1.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;   
            "Taco 1.8.8 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://yivesmirror.com/files/tacospigot/TacoSpigot-1.8.8.jar" -o server.jar 2> /dev/null > /dev/null
				break  
                ;;
            "Taco 1.11.2 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://yivesmirror.com/files/tacospigot/TacoSpigot-1.11.2-b102.jar" -o server.jar 2> /dev/null > /dev/null
				break                     
                ;;        
            "Taco 1.12.2 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://yivesmirror.com/files/tacospigot/TacoSpigot-1.12.2-b114.jar" -o server.jar 2> /dev/null > /dev/null
				break   
                ;;        
            "Vanilla 1.7.10 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/mc/game/1.7.10/server/952438ac4e01b4d115c5fc38f891710c4941df29/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;        
            "Vanilla 1.8.8 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/mc/game/1.8.8/server/5fafba3f58c40dc51b5c3ca72a98f62dfdae1db7/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;        
            "Vanilla 1.9.4 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/mc/game/1.9.4/server/edbb7b1758af33d365bf835eb9d13de005b1e274/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;      
            "Vanilla 1.10.2 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/mc/game/1.10.2/server/3d501b23df53c548254f5e3f66492d178a48db63/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;        
            "Vanilla 1.11.2 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/mc/game/1.11.2/server/f00c294a1576e03fddcac777c3cf4c7d404c4ba4/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;        
            "Vanilla 1.12.2 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/mc/game/1.12.2/server/886945bfb2b978778c3a0288fd7fab09d315b25f/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;     
            "Vanilla 1.13.2 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/v1/objects/3737db93722a9e39eeada7c27e7aca28b144ffa7/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;   
            "Vanilla 1.14.4 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/v1/objects/d0d0fe2b1dc6ab4c65554cb734270872b72dadd6/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;   
            "Vanilla 1.15 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/v1/objects/e9f105b3c5c7e85c7b445249a93362a22f62442d/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;
            "Vanilla 1.15.1 |")
                echo -e "You selected $REPLY) $opt"
				curl "https://launcher.mojang.com/v1/objects/4d1826eebac84847c71a77f9349cc22afd0cf0a1/server.jar" -o server.jar 2> /dev/null > /dev/null
				break
                ;;  
            "Nukkit |")
                echo -e "You selected $REPLY) $opt"
				curl "https://ci.nukkitx.com/job/NukkitX/job/Nukkit/job/master/lastSuccessfulBuild/artifact/target/nukkit-1.0-SNAPSHOT.jar" -o server.jar 2> /dev/null > /dev/null
				break           
                ;;  
            "Magma 1.12.2 |")
                echo -e "You selected $REPLY) $opt"
				curl -sSL -o server.jar https://api.magmafoundation.org/api/resources/magma/1.12.2/stable/latest/download
				break 
                ;;        
            "BungeeCord |")
                echo -e "You selected $REPLY) $opt"
				if [ -z "${BUNGEE_VERSION}" ] || [ "${BUNGEE_VERSION}" == "latest" ]; then
                BUNGEE_VERSION="lastStableBuild"
                fi
                curl -o server.jar https://ci.md-5.net/job/BungeeCord/${BUNGEE_VERSION}/artifact/bootstrap/target/BungeeCord.jar
				break 
                ;;        
            "Waterfall |")
                echo -e "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Waterfall/lastSuccessfulBuild/artifact/Waterfall-Proxy/bootstrap/target/Waterfall.jar" -o server.jar 2> /dev/null > /dev/null
				break   
                ;;        
            "NodeJS |")
                echo -e "You selected $REPLY) $opt"
				echo -e "NOTE: package.json file is required"
echo -e "| To run NodeJS  server, upload bot files and main file to be renamed to discord-bot.js"
                    sleep 10s
				break     
                ;;        
            "Python |")
                echo -e "You selected $REPLY) $opt"
				echo -e "NOTE: requirements.txt file is required"
echo -e "| To run Python3 server, upload bot file(s) and main file to be renamed to discord-bot.py and for dependencies add them in requirements.txt file"
                    sleep 10s
				break     
                ;;    
            "Travertine |")
                echo -e "You selected $REPLY) $opt"
				curl "https://papermc.io/ci/job/Travertine/lastSuccessfulBuild/artifact/Travertine-Proxy/bootstrap/target/Travertine.jar" -o server.jar 2> /dev/null > /dev/null
				break                                                                                                                                        
                ;; 
            "Velocity |")
                echo -e "You selected $REPLY) $opt"
				if [ -z ${VELOCITY_VERSION} ] || [ ${VELOCITY_VERSION} == "latest" ]; then
                VELOCITY_VERSION=/lastStableBuild
                fi
                DOWNLOAD_ENDPOINT=$(curl https://ci.velocitypowered.com/job/velocity/${VELOCITY_VERSION}/ | grep -Eo 'href="[^\"]+"' | grep -vE "view|fingerprint" | grep ".jar" | sed -n 's/.*href="\([^"]*\).*/\1/p')
                DOWNLOAD_LINK=https://ci.velocitypowered.com/job/velocity/lastStableBuild/${DOWNLOAD_ENDPOINT}
                curl ${DOWNLOAD_LINK} -o server.jar
				break     
                ;;        
            "PMMP |")
                echo -e "You selected $REPLY) $opt"
                curl -sSL -o PocketMine-MP.phar  https://jenkins.pmmp.io/job/PocketMine-MP/lastSuccessfulBuild/artifact/PocketMine-MP.phar
                curl -sSL -o php.binary.tar.gz https://jenkins.pmmp.io/job/PHP-7.3-Linux-x86_64/lastSuccessfulBuild/artifact/PHP_Linux-x86_64.tar.gz
                tar -xzvf php.binary.tar.gz
                rm -rf php.binary.tar.gz
                touch banned-ips.tx banned-players.txt ops.txt white-list.txt server.log
                mkdir -p players worlds plugins resource_packs
				break
                ;;   
            "Cuberite |")
                echo -e "You selected $REPLY) $opt"
                wget https://builds.cuberite.org/job/Cuberite%20Linux%20x64%20Master/lastSuccessfulBuild/artifact/Cuberite.tar.gz
                tar --strip-components=1 -xf Cuberite.tar.gz
                rm Cuberite.tar.gz
				break
                ;;       
            "Vanilla Bedrock |")
                echo -e "You selected $REPLY) $opt"
				BEDROCK_VERSION=latest
if [ -z "${BEDROCK_VERSION}" ] || [ "${BEDROCK_VERSION}" == "latest" ]; then
    echo -e "\n Downloading latest Bedrock server"
    DOWNLOAD_URL=$(curl https://www.minecraft.net/en-us/download/server/bedrock/ | grep azureedge | grep linux | grep -Eo "(http|https)://[a-zA-Z0-9./?=_-]*")
    BEDROCK_ZIP=$(echo ${DOWNLOAD_URL} | cut -d"/" -f5)
else 
    echo -e "\n Downloading ${BEDROCK_VERSION} Bedrock server"
    DOWNLOAD_URL=https://minecraft.azureedge.net/bin-linux/bedrock-server-$BEDROCK_VERSION.zip
    BEDROCK_ZIP=$(echo ${DOWNLOAD_URL} | cut -d"/" -f5)
fi

wget ${DOWNLOAD_URL}
unzip -o $BEDROCK_ZIP
rm $BEDROCK_ZIP
				break
                ;;   		                  			                                                        
            "PocketNodeX |")
                echo -e "You selected $REPLY) $opt"
				git clone https://github.com/HerryYT/PocketNodeX.git
                    cd PocketNode
                    npm install --unsafe-perm
                    nexe index.js
                    mv PocketNode /home/container
                    rm -rf /home/container/PocketNode
                    chmod u+x /home/container/PocketNode
				break
                ;;   		                  			                                                        
            "Custom")
                echo -e "You selected $REPLY) $opt"
				echo "Upload your custom jar via File Management and rename it to server.jar"
				sleep 1m
				break
                ;;
            *) echo -e "invalid option $REPLY";;            
        esac
    done
	JAR="server.jar"
fi
echo -e "=========Starting========="
java -Xms128M -Xmx"$SERVER_MEMORY"M -Dterminal.jline=false -Dterminal.ansi=true -jar "$JAR"

    