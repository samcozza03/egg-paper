FROM python:3.7.7-slim-stretch

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
    && apt-get -y install apt-utils curl jq zip unzip libopus0 ffmpeg youtube-dl software-properties-common apt-transport-https ca-certificates wget dirmngr gnupg iproute2 make g++ locales git cmake \
    && useradd -d /home/container -m container

    # Ensure UTF-8
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

    # Java 8
RUN mkdir -p /usr/share/man/man1 && wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add - && add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/ 
RUN apt-get update && apt-get -y install adoptopenjdk-8-hotspot
RUN java -version

    # NodeJS
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - \
    && apt-get update \
    && apt-get -y install libopus0 nodejs npm node-gyp \
    && npm install discord.js node-opus opusscript \
    && npm install sqlite3 --build-from-source 
RUN npm i npm@latest -g
RUN npm i nexe -g
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get -y install yarn
RUN npm install -g nodemon && nodemon -v

    # Python3
RUN apt-get update && apt-get -y install libffi-dev \
    && pip3 install -q aiohttp websockets pynacl opuslib \
    && python3 -m pip install -U discord.py[voice]
RUN apt-get update && apt-get -y install dnsutils build-essential

USER container
ENV  USER container
ENV  HOME /home/container

WORKDIR /home/container

COPY ./minecraft.sh /minecraft.sh
COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]
